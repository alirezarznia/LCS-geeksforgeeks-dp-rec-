#include 	<cstdio>
#include	<iostream>
#include	<cmath>
#include 	<cstring>
#include 	<cstdlib>
#include 	<vector>
#include 	<string>
#include 	<algorithm>
#include 	<queue>
#include 	<deque>
#include 	<set>
#include 	<stack>
#include 	<map>
#include 	<sstream>
#include 	<ctime>
#include	<iomanip>
#include 	<functional>

#define 	Time 		printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K) 	for(ll J=R;J<K;++J)
#define 	Rep(I,N) 	For(I,0,N)
#define 	MP 			make_pair
#define 	ALL(X) 		(X).begin(),(X).end()
#define 	SF 			scanf
#define 	PF 			printf
#define 	pii 		pair<long long,long long>
#define 	pdd 		pair<double , double>
#define 	Sort(v) 	sort(ALL(v))
#define 	Test 		freopen("a.in","r",stdin)
#define 	Testout 	freopen("a.out","w",stdout)
#define 	pb 			push_back
#define 	Set(a,n) 	memset(a,n,sizeof(a))
#define 	MAXN 		100000+99
#define 	EPS 		1e-15
#define 	inf 		1ll<<62

typedef long long ll;

using namespace std;
//
string s1, s2;
ll dp[101][101];
ll LCS(ll n , ll m)
{
    if(m== 0 || n==0)
        return 0;
    if(dp[n][m] != -1)
        return dp[n][m];
    if(s1[n-1] == s2[m-1])
        return dp[n][m] = 1+LCS(n-1 ,m-1);
    return dp[n][m]=max(LCS(n-1  ,m) , LCS(n  ,m -1));
}
int main()
{
    //Test;
    ll t;
    cin>> t;
    while(t--)
    {
        Set(dp , -1);
        ll n , m;
        cin >>n >> m;
        cin>>s1>>s2;
        cout<<LCS(n ,m) <<endl;

    }
}
